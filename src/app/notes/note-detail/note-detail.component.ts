import { Component, Input } from '@angular/core';

import { NoteService } from '../note.service';
import { Router } from '@angular/router';

import { Note } from '../note-model';

@Component({
  selector: 'note-detail',
  templateUrl: './note-detail.component.html',
  styleUrls: ['./note-detail.component.scss'],
})
export class NoteDetailComponent {

  @Input()
  note: Note;

  constructor(private noteService: NoteService, private router: Router) { }

  addHeartToNote(val: number) {
    if (this.note.id) {
      this.noteService.updateNote(this.note.id, { hearts: val + 1 });
    } else {
      console.error('Note missing ID!');
    }
  }
  viewDetails(val: any) {
    if (this.note.id) {
      localStorage.setItem('projectkey', val.id);
      this.router.navigate(['/order', { id: val.id}]);
    } else {
      console.error('Note missing ID!');
    }
  }
  deleteNote(id: string) {
    this.noteService.deleteNote(id);
  }

}
