import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { SharedModule } from '../shared/shared.module';

import { NoteService } from './note.service';
import { OrderService } from '../order/shared/order.service';

import { NotesListComponent } from './notes-list/notes-list.component';
import { NoteDetailComponent } from './note-detail/note-detail.component';

import { AngularFirestoreModule } from 'angularfire2/firestore';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { LoadingModule } from 'ngx-loading';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModule,
    SharedModule,
    AngularFirestoreModule.enablePersistence(),
    LoadingModule,
    ToastrModule.forRoot(),
  ],
  declarations: [
    NotesListComponent,
    NoteDetailComponent,
  ],
  providers: [NoteService, OrderService],
})
export class NotesModule { }
