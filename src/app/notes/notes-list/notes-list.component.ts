import { Component, OnInit } from '@angular/core';

import { NoteService } from '../note.service';
import { OrderService } from '../../order/shared/order.service';

import { Note } from '../note-model';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs/Observable';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
var accountSid = 'ACXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'; // Your Account SID from www.twilio.com/console
var authToken = 'your_auth_token';   // Your Auth Token from www.twilio.com/console

var twilio = require('twilio');
@Component({
  selector: 'notes-list',
  templateUrl: './notes-list.component.html',
  styleUrls: ['./notes-list.component.scss'],
})
export class NotesListComponent implements OnInit {

  notes: Observable<Note[]>;
  orders: Observable<any>;
  onedays: Observable<any>;
  selectedOrder: any;
  content: string;
  book: any;
  selectedDate: string;
  images: any[];
  countris: any[];
  constructor(private noteService: NoteService, private modalService: NgbModal,
     private orderService: OrderService,  private toastr: ToastrService) {
    this.orders = this.orderService.getOrdersList();
    this.onedays = this.orderService.getOneDaysList();
    this.book = {};
    this.images = [{ 'src': '../assets/img/2013.jpg', 'year': '2013' },
    { 'src': '../assets/img/20131.jpg', 'year': '2013' },
    { 'src': '../assets/img/20132.jpg', 'year': '2013' },
    { 'src': '../assets/img/20132.jpg', 'year': '2013' },
    { 'src': '../assets/img/20133.jpg', 'year': '2013' },
    { 'src': '../assets/img/20135.jpg', 'year': '2013' },
    { 'src': '../assets/img/20136.jpg', 'year': '2013' },
    { 'src': '../assets/img/20137.jpg', 'year': '2013' }];
  }

  ngOnInit() {
    // this.notes = this.noteService.getData()
    this.notes = this.noteService.getSnapshot();
    this.orders.subscribe((orderArr) => { console.log('order'); });
    this.onedays.subscribe((orderArr) => { console.log('order'); });
  }

  createNote() {
    this.noteService.create(this.content);
    this.content = '';
  }

  bookNow(content: any, order: any) {
    this.selectedDate = order.description;
    this.selectedOrder = order;
    this.modalService.open(content, { windowClass: 'dark-modal' });
  }
  booking(content: any, book: any) {
    console.log(book);
    book.datekey = this.selectedOrder['$key'];
    book.date = this.selectedOrder['description'];
    if (!(book.count)) {
      book.count = 1;
    }
    console.log(book);
    this.orderService.createBooking(book);
    this.modalService.open(content, { windowClass: 'dark-modal' });
    this.book = {};
  }
}
