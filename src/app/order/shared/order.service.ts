import { Injectable } from '@angular/core';

import { AngularFireDatabase, AngularFireList, AngularFireObject } from 'angularfire2/database';

import { Order } from './order';

import { Observable } from 'rxjs/Observable';

@Injectable()
export class OrderService {

  private onedayPath = '/onedays';
  private orderPath = '/orders';
  private bookingPath = '/bookings';

  ordersRef: AngularFireList<Order>;
  orderRef: AngularFireObject<Order>;
  onedaysRef: AngularFireList<Order>;
  onedayRef: AngularFireObject<Order>;
  profitRef: AngularFireObject<Order>;

  bookingsRef: AngularFireList<any>;
  bookingRef: AngularFireObject<Order>;
  bookingsListRef: AngularFireList<any>;
  bookingListRef: AngularFireObject<Order>;
  constructor(private db: AngularFireDatabase) {
    this.ordersRef = db.list('/orders');
    this.onedaysRef = db.list('/onedays');
  }

  // Return an observable list of Orders
  getOrdersList(): Observable<Order[]> {
    const orderRefString = `${this.orderPath}`;
    this.ordersRef = this.db.list(orderRefString);
    return this.ordersRef.snapshotChanges().map((arr) => {
      return arr.map((snap) => Object.assign(snap.payload.val(), { $key: snap.key }));
    });
  }
  getOneDaysList(): Observable<Order[]> {
    const onedayRefString = `${this.onedayPath}`;
    this.onedaysRef = this.db.list(onedayRefString);
    return this.onedaysRef.snapshotChanges().map((arr) => {
      return arr.map((snap) => Object.assign(snap.payload.val(), { $key: snap.key }));
    });
  }
  getbookingList(key: any): Observable<any[]> {
    const bookinglistRefString = `${this.bookingPath}/${key}`;
    this.bookingsListRef = this.db.list(bookinglistRefString);
    return this.bookingsListRef.snapshotChanges().map((arr) => {
      return arr.map((snap) => Object.assign(snap.payload.val(), { $key: snap.key }));
    });
  }
  // Return a single observable Order
  getOrder(key: string): Observable<Order | null> {
    const orderRefString = `${this.orderPath}`;
    const orderPath = `${orderRefString}/${key}`;
    const order = this.db.object(orderPath).valueChanges() as Observable<Order | null>;
    return order;
  }

  // Create a brand new Order
  createOrder(order: Order): void {
    const orderRefString = `${this.orderPath}`;
    this.ordersRef = this.db.list(orderRefString);
    this.ordersRef.push(order);
  }
  createOneDayOrder(order: Order): void {
    const orderRefString = `${this.onedayPath}`;
    this.onedaysRef = this.db.list(orderRefString);
    this.onedaysRef.push(order);
  }

  createBooking(order: any): void {
    const bookingRefString = `${this.bookingPath}/${order.datekey}`;
    this.bookingsRef = this.db.list(bookingRefString);
    this.bookingsRef.push(order);
  }
  // Update an exisiting Order
  updateOrder(key: string, value: any): void {
    const orderRefString = `${this.orderPath}`;
    this.ordersRef = this.db.list(orderRefString);
    this.ordersRef.update(key, value);
  }

  updateBookingOrder(key: any, value: any) {
    const projectkey = localStorage.getItem('projectkey');
    const orderRefString = `${this.orderPath}/${key}/booking`;
    this.orderRef = this.db.object(orderRefString);
    this.orderRef.set(value);
  }
  updateBookingDayOrder(key: any, value: any) {
    const orderRefString = `${this.onedayPath}/${key}/booking`;
    this.onedayRef = this.db.object(orderRefString);
    this.onedayRef.set(value);
  }
  // Deletes a single Order
  deleteOrder(key: string): void {
    const orderRefString = `${this.orderPath}`;
    this.ordersRef = this.db.list(orderRefString);
    this.ordersRef.remove(key);
  }
  deleteBooking(key: string, bookkey: string): void {
    const bookinglistRefString = `${this.bookingPath}/${key}`;
    this.bookingsListRef = this.db.list(bookinglistRefString);
    this.bookingsListRef.remove(bookkey);
  }
  deleteOneDay(key: string): void {
    const onedayRefString = `${this.onedayPath}`;
    this.onedaysRef = this.db.list(onedayRefString);
    this.onedaysRef.remove(key);
  }
  // Deletes the entire list of Orders
  deleteAll(): void {
    this.ordersRef.remove();
  }

  // Default error handling for all actions
  private handleError(error: Error) {
    console.error(error);
  }
}
