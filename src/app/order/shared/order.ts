export class Order {
  $key: string;
  title: string;
  date: string;
  month: string;
  description: string;
  active = true;
  booking = false;
  timeStamp: number;
}
