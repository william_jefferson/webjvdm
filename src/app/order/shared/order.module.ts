import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AngularFireDatabaseModule } from 'angularfire2/database';

import { SharedModule } from '../../shared/shared.module';

import { OrderService } from './order.service';

import { OrderListComponent } from '../order-list/order-list.component';
import { OrderFormComponent } from '../order-form/order-form.component';
import { OrderDetailComponent } from '../order-detail/order-detail.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoadingModule } from 'ngx-loading';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule,
    LoadingModule,
    ToastrModule.forRoot(),
    AngularFireDatabaseModule,
  ],
  declarations: [
    OrderListComponent,
    OrderFormComponent,
    OrderDetailComponent,
  ],
  providers: [
    OrderService,
  ],
})
export class OrderModule { }
