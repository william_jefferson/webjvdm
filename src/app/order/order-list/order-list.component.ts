import { Component, OnInit } from '@angular/core';
import { OrderService } from '../shared/order.service';
import { Order } from '../shared/order';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss'],
})
export class OrderListComponent implements OnInit {

  orders: Observable<any>;
  onedays: Observable<any>;
  profitRef: Observable<any>;
  showSpinner = true;
  totalprice = 0;
  totalActualprice = 0;
  profit = 0;
  totalProfit = 0;
  bookings: Observable<any>;
  selectedbookings: any;
  constructor(private orderService: OrderService,
    private modalService: NgbModal,
    private activatedRoute: ActivatedRoute, private router: Router) {
    this.orders = this.orderService.getOrdersList();
    this.onedays = this.orderService.getOneDaysList();
    this.activatedRoute.params.subscribe((params: Params) => {
      console.log(params);
    });
  }

  getOrdersList() {
    this.orders = this.orderService.getOrdersList();
  }
  updateStatus(order: any) {
    console.log(order);
    const key = order['$key'];
    order.booking = !order.booking;
    this.orderService.updateBookingOrder(key, order.booking);
  }
  updateStatusOneDay(order: any) {
    const key = order['$key'];
    order.booking = !order.booking;
    this.orderService.updateBookingDayOrder(key, order.booking);
  }
  ngOnInit() {
    this.orders.subscribe((orderArr) => {
      this.showSpinner = false;
    });
    this.onedays.subscribe((orderArr) => {
      this.showSpinner = false;
    });
  }

  viewOrder(order: any) {
    console.log(order);
    localStorage.setItem('orderkey', order['$key']);
    this.router.navigateByUrl('/items');
  }
  deleteOrder(order: any) {
    this.orderService.deleteOrder(order.$key);
  }
  deleteOrders() {
    this.orderService.deleteAll();
  }
  deleteOneDay(order: any) {
    this.orderService.deleteOneDay(order.$key);
  }
  deleteBooking(book: any) {
    this.orderService.deleteBooking(this.selectedbookings.$key, book.$key);
  }
  createCamp(content: any) {
    this.modalService.open(content, { windowClass: 'dark-modal' });
  }
  detailsBooking(content: any, order: any) {
    this.selectedbookings = order;
    this.bookings = this.orderService.getbookingList(order.$key);
    this.bookings.subscribe((orderArr) => {
      this.totalprice = 0;
      this.showSpinner = false;
      for (let i = 0; i < orderArr.length; i++) {
        this.totalprice += parseInt(orderArr[i].count, 10);
      }
    });
    this.modalService.open(content, { windowClass: 'dark-modal' });
  }
}
