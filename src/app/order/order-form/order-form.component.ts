import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { Order } from '../shared/order';

import { ToastrService } from 'ngx-toastr';

import { OrderService } from '../shared/order.service';

@Component({
  selector: 'order-form',
  templateUrl: './order-form.component.html',
  styleUrls: ['./order-form.component.scss'],
})
export class OrderFormComponent {

  order: Order = new Order();
  public loading = false;

  constructor(private orderSvc: OrderService, private toastr: ToastrService) { }
  createOrder() {
    this.loading = true;
    this.order.booking = false;
    this.order.active = true;
     this.toastr.success('Hello world!', 'Toastr fun!');
    this.orderSvc.createOrder(this.order);
    this.loading = false;
    this.order = new Order(); // reset order
  }
}
