import { Component, Input } from '@angular/core';
import { OrderService } from '../shared/order.service';
import { Order } from '../shared/order';

@Component({
  selector: 'order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.scss'],
})
export class OrderDetailComponent {
   order: Order = new Order();
  constructor(private orderSvc: OrderService) { }
  createOrder() {
    this.order.booking = false;
    this.order.active = true;
    this.orderSvc.createOneDayOrder(this.order);
    this.order = new Order(); // reset order
  }

}
