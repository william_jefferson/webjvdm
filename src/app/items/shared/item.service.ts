import { Injectable } from '@angular/core';

import { AngularFireDatabase, AngularFireList, AngularFireObject } from 'angularfire2/database';

import { Item } from './item';

import { Observable } from 'rxjs/Observable';

@Injectable()
export class ItemService {
  private notesPath = '/notes';
  private orderPath = '/orders';
  private basePath = '/items';

  itemsRef: AngularFireList<Item>;
  itemRef: AngularFireObject<Item>;
  orderRef: AngularFireObject<Item>;

  constructor(private db: AngularFireDatabase) {
    this.itemsRef = db.list('/items');
  }

  // Return an observable list of Items
  getItemsList(): Observable<Item[]> {
    const projectkey = localStorage.getItem('projectkey');
    const orderkey = localStorage.getItem('orderkey');
    const itemsRefString = `${this.notesPath}/${projectkey}/${this.orderPath}/${orderkey}/${this.basePath}`;
    this.itemsRef = this.db.list(itemsRefString);
    return this.itemsRef.snapshotChanges().map((arr) => {
      return arr.map((snap) => Object.assign(snap.payload.val(), { $key: snap.key }));
    });
  }

  // Return a single observable item
  getItem(key: string): Observable<Item | null> {
    const projectkey = localStorage.getItem('projectkey');
    const orderkey = localStorage.getItem('orderkey');
    const itemsRefString = `${this.notesPath}/${projectkey}/${this.orderPath}/${orderkey}/${this.basePath}`;
    const itemPath = `${itemsRefString}/${key}`;
    const item = this.db.object(itemPath).valueChanges() as Observable<Item | null>;
    return item;
  }

  // Create a brand new item
  createItem(item: Item): void {
    const projectkey = localStorage.getItem('projectkey');
    const orderkey = localStorage.getItem('orderkey');
    const itemsRefString = `${this.notesPath}/${projectkey}/${this.orderPath}/${orderkey}/${this.basePath}`;
    this.itemsRef = this.db.list(itemsRefString);
    this.itemsRef.push(item);
  }

  // Update an exisiting item
  updateItem(key: string, value: any): void {
    const projectkey = localStorage.getItem('projectkey');
    const orderkey = localStorage.getItem('orderkey');
    const itemsRefString = `${this.notesPath}/${projectkey}/${this.orderPath}/${orderkey}/${this.basePath}`;
    this.itemsRef = this.db.list(itemsRefString);
    this.itemsRef.update(key, value);
  }
  updateOrder(key: string, value: any): void {
    const projectkey = localStorage.getItem('projectkey');
    const orderkey = localStorage.getItem('orderkey');
    const orderRefString = `${this.notesPath}/${projectkey}${this.orderPath}/${orderkey}/${key}`;
    console.log(orderRefString);
    this.orderRef = this.db.object(orderRefString);
    this.orderRef.set(value);
  }

  // Deletes a single item
  deleteItem(key: string): void {
    const projectkey = localStorage.getItem('projectkey');
    const orderkey = localStorage.getItem('orderkey');
    const itemsRefString = `${this.notesPath}/${projectkey}/${this.orderPath}/${orderkey}/${this.basePath}`;
    this.itemsRef = this.db.list(itemsRefString);
    this.itemsRef.remove(key);
  }

  // Deletes the entire list of items
  deleteAll(): void {
    const projectkey = localStorage.getItem('projectkey');
    const orderkey = localStorage.getItem('orderkey');
    const itemsRefString = `${this.notesPath}/${projectkey}/${this.orderPath}/${orderkey}/${this.basePath}`;
    this.itemsRef = this.db.list(itemsRefString);
    this.itemsRef.remove();
  }

  // Default error handling for all actions
  private handleError(error: Error) {
    console.error(error);
  }
}
