import { Component, OnInit } from '@angular/core';

import { ItemService } from '../shared/item.service';

import { Item } from '../shared/item';

import { Observable } from 'rxjs/Observable';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'items-list',
  templateUrl: './items-list.component.html',
  styleUrls: ['./items-list.component.scss'],
})
export class ItemsListComponent implements OnInit {

  items: Observable<any>;
  showSpinner = true;
  totalprice = 0;
  totalbuyingprice = 0;
  item: Item = new Item();
  constructor(private itemService: ItemService, private modalService: NgbModal) {
    this.items = this.itemService.getItemsList();

  }
  createItems(content: any) {
    this.modalService.open(content, { windowClass: 'dark-modal' });
  }
  createItem() {
    this.itemService.createItem(this.item);
    this.item = new Item(); // reset item
  }
  ngOnInit() {
    this.items.subscribe((itemArr) => {
      this.totalprice = 0;
      this.showSpinner = false;
      for (const item of itemArr) {
        this.totalprice += (parseFloat(item.price) * parseFloat(item.qty));
        if (!(item.buyingprice)) {
          item.buyingprice = 0;
        }
        this.totalbuyingprice += (parseFloat(item.buyingprice) * parseFloat(item.qty));
      }
      console.log(this.totalprice);
      this.itemService.updateOrder('price', this.totalprice);
      this.itemService.updateOrder('buyingtotalprice', this.totalbuyingprice);
    });
  }
  editItem(content: any, item: any) {
    this.item = item;
    this.modalService.open(content, { windowClass: 'dark-modal' });
  }
  deleteItems() {
    this.itemService.deleteAll();
  }
  updateItem(item: any) {
    if (!item.buyingprice) {
      item.buyingprice = 0;
    }
    const key = item['$key'];
    delete item['$key'];
    this.itemService.updateItem(key, item);
    console.log(item);
    this.items = this.itemService.getItemsList();
  }
}
